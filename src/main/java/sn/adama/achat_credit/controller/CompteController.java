package sn.adama.achat_credit.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import sn.adama.achat_credit.entites.Compte;
import sn.adama.achat_credit.service.CompteService;

@RestController
@RequestMapping("/gestion")
public class CompteController {
    @Autowired
    private CompteService compteService;
    @PostMapping("/compte/add-compte")
    public Compte save(@RequestBody Compte compte) {
        return compteService.saveCompte(compte);
    }
}
