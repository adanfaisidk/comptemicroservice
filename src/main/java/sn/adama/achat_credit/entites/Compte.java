package sn.adama.achat_credit.entites;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;

@Entity
@Data
@NoArgsConstructor
@AllArgsConstructor
public class Compte implements Serializable {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    private Double solde_veille;
    private String numero_compte;
    private Double balance;
    private String type_compte;
    private final Date date_ouverture = new Date();
    private Date blocage_compte;
    private Double solde_plafond;
    private String nomUtilisateur;


}
