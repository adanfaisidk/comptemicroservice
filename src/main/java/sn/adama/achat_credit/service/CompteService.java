package sn.adama.achat_credit.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;
import sn.adama.achat_credit.dto.UtilisateurDto;
import sn.adama.achat_credit.entites.Compte;
import sn.adama.achat_credit.repository.CompteRepository;

@Service
public class CompteService {
    @Autowired
    private CompteRepository compteRepository;
    @Autowired
    private RestTemplate restTemplate;
    public Compte saveCompte(Compte compte){
        String responseTest = "";
         UtilisateurDto userDto = restTemplate.getForObject("http://UTILISATEUR-SERVICE/api/getUserByNom/{id}", UtilisateurDto.class,compte.getNomUtilisateur());
        assert userDto != null;
        compte.setNomUtilisateur(userDto.getNom());
        System.out.println("RESPOOOOOOOONSE : "+userDto);
        return compteRepository.save(compte);
    }

}
